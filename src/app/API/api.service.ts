import { Injectable }                         from '@angular/core';
import { HttpClient, HttpHeaders }            from '@angular/common/http';
import { Observable,throwError }              from 'rxjs';
import { retry, catchError }                  from 'rxjs/operators';
import { Post } from '../types/post';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  private URL = 'https://jsonplaceholder.typicode.com/';

  getPosts(): Observable<Object> {
    return this.http.get(this.URL+"posts").pipe(
      retry(1),
      catchError(this.handleError)
  );
  }

  getPostDetail(id:number): Observable<object> {
    return this.http.get(this.URL+"posts/"+id).pipe(
      retry(1),
      catchError(this.handleError)
  );;
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
}

  getComments(id:number): Observable<object>{
    return this.http.get(this.URL+"posts/"+id+"/comments").pipe(
      retry(1),
      catchError(this.handleError)
  );
  }

  deletePost(id:number): Observable<any>{
    return this.http.delete(this.URL+"posts/"+id,{ responseType: 'text' }).pipe(
      retry(1),
      catchError(this.handleError)
  );
  }
  submitPost(post:Post): Observable<any>{
  return this.http.post(this.URL+"posts/",
                        JSON.stringify({
                          title: post.title,
                          body: post.body,
                          userId: post.userId
                        }),
                        { headers: {"Content-type": "application/json; charset=UTF-8"}}).pipe(
                              retry(1),
                              catchError(this.handleError)
                          );;
  
  }
}
