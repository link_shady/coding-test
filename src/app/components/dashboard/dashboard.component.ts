import { Component, OnInit } from '@angular/core';
import { ApiService }        from "../../API/api.service";
import { AddPostComponent } from "../add-post/add-post.component";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private api:ApiService) { }
  posts:[];
  public searchText:string;

  ngOnInit(): void {
    this.getPosts();
  }

  getPosts():void{
    this.api.getPosts().subscribe((posts:[]) => (this.posts = posts));
  }

  deletePost(index,postId){
    console.log(index);
    this.api.deletePost(postId).subscribe((result) => (this.posts.splice(index,1)));
  }
}
