import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent }   from './components/dashboard/dashboard.component';
import { DetailsComponent }     from './components/details/details.component';
import { AddPostComponent } from "./components/add-post/add-post.component";


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
  path:"dashboard",
  component:DashboardComponent
  },
  {
    path:"post/:id",
    component:DetailsComponent
  },
  {
    path:"addPost",
    component:AddPostComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
