import { Component, OnInit, Input } from '@angular/core';
import { Post } from "../../types/post";
import { ApiService } from 'src/app/API/api.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  constructor(private api:ApiService) { }
  @Input() posts:[object];
  showNewPostForm:boolean;
  post:Post;
  userId:number;
  id:number;
  title:string;
  body:string;

  ngOnInit(): void {
  }
  showForm():void{
    this.showNewPostForm = !this.showNewPostForm;
  }
  addToArray(userId?:number,title?:string,body?:string):void{
    const post = {
      userId: userId ? userId : 1,
      id    : this.posts.length + 1,
      title : title ? title : "No title",
      body  : body ? body : " "
    }
    this.api.submitPost(post).subscribe((result) => this.posts.push(post));
  }
  
}
