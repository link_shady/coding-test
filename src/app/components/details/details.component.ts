import { Component, OnInit } from '@angular/core';
import { ApiService }        from "../../API/api.service";
import { ActivatedRoute }    from '@angular/router';
import { Location }          from '@angular/common';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private api    : ApiService,
              private route  : ActivatedRoute) { }
  
  //  variables to display
  post    : Object
  comments: Object

  // variable to decide if we should show the comments
  showCommentsSection: boolean = true;

  ngOnInit(): void {
    this.getPost();
    this.getComments();
  }

  getPost():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.api.getPostDetail(id).subscribe((post) => (this.post = post));
  }
  
  getComments():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.api.getComments(id).subscribe((comments) => (this.comments = comments));
  }

  showComments():void {
    this.showCommentsSection = !this.showCommentsSection;
  }
}
